[[_TOC_]]

# Prueba Técnica **UBITS** - SW Engineer

## Instrucciones

* To install this project, your server needs the [Laravel](https://laravel.com/) [requirements](https://laravel.com/docs/7.x#server-requirements)
* Configure your database variables in the [.env](.env) file 
```env
DB_DATABASE=your_database
DB_USERNAME=you_username
DB_PASSWORD=your_password
```

* Run the folloging command from your console
```bash
composer install
php artisan migrate
php artisan db:seed
```
* The user you will be using is
  * user: *ubits@ubits.com*
  * password: *secret*
* There will be several users with random email and all of these have the password *`secret`*

## Unit Test

### LoginTest

* *testUserCanViewALoginForm*
* *testUserCanLogin*
* *testUserCanLoginWithCorrectCredentials*
* *testUserCannotLoginWithIncorrectPassword* 

## Propuesta de Arquitectura AWS

![](./aws/awsArquitecture.png)
