<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{

    protected $fillable = [
        'name',
        'type',
        'size',
        'description',
        'age'
    ];

    public function vaccines()
    {
        return $this->belongsToMany(Vaccine::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function spas()
    {
        return $this->hasMany(Spa::class);
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
