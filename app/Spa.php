<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spa extends Model
{
    protected $fillable = [
        'type', 'date',
    ];

    public function pet()
    {
        return $this->hasOne(Pet::class);
    }
}
