<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'type', 'date',
    ];

    public function pet()
    {
        return $this->hasOne(Pet::class);
    }
}
