<?php

namespace App\Http\Controllers;

use App\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MedicalServicesController extends Controller
{

    public function medicalServices()
    {
        return view('medical-services', [
            'user'         => Auth::user(),
            'pet'          => Auth::user()->pet,
            'appointments' => Auth::user()->pet->appointments()->get()
        ]);
    }

    public function medicalServicesSave( Request $request )
    {
        $pet = Pet::find(decrypt($request->get('pet_id')));
        foreach ($request->get('medicalService') as $service) {
            $pet->appointments()
                ->create([
                    'type' => $service,
                    'date' => $request->get('date')
                ]);
        }
        return redirect(route('medicalServices'));
    }
}
