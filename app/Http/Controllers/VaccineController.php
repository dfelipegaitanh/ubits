<?php

namespace App\Http\Controllers;

use App\Pet;
use App\Vaccine;
use Illuminate\Http\Request;

class VaccineController extends Controller
{

    public function addVaccine( Request $request )
    {
        $pet     = Pet::find(decrypt($request->get('pet_id')));
        $vaccine = Vaccine::find(( decrypt($request->get('vaccine_id')) ));
        $pet->vaccines()->attach($vaccine);

        return redirect(route('home'));
    }

}
