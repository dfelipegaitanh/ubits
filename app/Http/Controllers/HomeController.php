<?php

namespace App\Http\Controllers;

use App\Pet;
use App\Vaccine;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function medicalRecord()
    {
        return view('medical-record', [
            'user'        => Auth::user(),
            'pet'         => Auth::user()->pet,
            'vaccines'    => Auth::user()->pet->vaccines,
            'allVaccines' => Vaccine::all()
        ]);
    }

}
