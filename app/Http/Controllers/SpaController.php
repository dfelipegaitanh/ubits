<?php

namespace App\Http\Controllers;

use App\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SpaController extends Controller
{

    public function spaServices()
    {
        return view('spa-services', [
            'user' => Auth::user(),
            'pet'  => Auth::user()->pet,
            'spas' => Auth::user()->pet->spas()
                                       ->get()
        ]);
    }

    public function spaServicesSave( Request $request )
    {
        $pet = Pet::find(decrypt($request->get('pet_id')));
        foreach ($request->get('spaService') as $spa) {
            $pet->spas()
                ->create([
                    'type' => $spa,
                    'date' => $request->get('date')
                ]);
        }
        return redirect(route('spaServices'));
    }
}
