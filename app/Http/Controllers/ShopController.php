<?php

namespace App\Http\Controllers;

use App\Pet;
use App\Shop;
use App\Spa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{

    public function spaServices()
    {
        return view('shop-services', [
            'user'  => Auth::user(),
            'pet'   => Auth::user()->pet,
            'shops' => Auth::user()->pet->shops()
                                        ->get()
        ]);
    }

    public function spaServicesSave( Request $request )
    {
        $pet = Pet::find(decrypt($request->get('pet_id')));
        foreach ($request->get('shopService') as $shop) {
            $pet->shops()
                ->create([
                    'type' => $shop,
                    'date' => $request->get('date')
                ]);
        }
        return redirect(route('shopServices'));
    }

}
