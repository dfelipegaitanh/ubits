<?php

namespace App\Http;

use App\Appointment;
use App\Pet;
use App\Shop;
use App\Spa;
use Illuminate\Support\Facades\Auth;

class Helpers
{
    public static function getBadgeMedicalServices()
    {
        $count = Pet::find(Auth::user()->pet->id)->appointments()->count();
        if ( $count ) {
            echo '<span class="badge badge-pill badge-danger">' . $count . '</span>';
        }
    }

    public static function getBadgeSpas()
    {
        $count = Pet::find(Auth::user()->pet->id)->spas()->count();
        if ( $count ) {
            echo '<span class="badge badge-pill badge-danger">' . $count . '</span>';
        }
    }

    public static function getBadgeShops()
    {
        $count = Pet::find(Auth::user()->pet->id)->shops()->count();
        if ( $count ) {
            echo '<span class="badge badge-pill badge-danger">' . $count . '</span>';
        }
    }

    public static function getBadgeVaccines()
    {
        $count = Auth::user()->pet->vaccines->count();
        if ( $count ) {
            echo '<span class="badge badge-pill badge-danger">' . $count . '</span>';
        }
    }

}
