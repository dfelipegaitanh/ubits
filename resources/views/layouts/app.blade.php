<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('templates.head')

<body>
<div class="page-wrapper default-theme sidebar-bg bg1 toggled">
    @yield('modal')
    @include("templates.sidebar")
    <main class="page-content pt-2">
        <div id="overlay" class="overlay"></div>
        <div class="container-fluid p-5">
            @yield('content')
        </div>
    </main>
</div>
</body>

</html>
