<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('templates.head')

<body>
    <header class="dark-green"></header>
    <div class="diagonal-green">
        <div id="app">
            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </div>
</body>

</html>
