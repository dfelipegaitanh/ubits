<nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">

        <div class="sidebar-item sidebar-header d-flex flex-nowrap">
            <div class="rewow-text"><i class="fa fa-paw"></i> Re Wow</div>
        </div>

        <div class=" sidebar-item sidebar-menu">
            <ul>
                <li class="header-menu"></li>
                <li class="sidebar-dropdown">
                    <a href="{{ route('medical-record') }}" class="@if (Request::is('medical-record')) {{'active'}} @endif">
                        <i class="fa fa-paw" aria-hidden="true"></i>
                        <span class="menu-text">Historia</span>
                        {{ App\Http\Helpers::getBadgeVaccines() }}
                    </a>
                </li>
                <li class="sidebar-dropdown">
                    <a href="{{ route('medicalServices') }}" class="@if (Request::is('medicalServices')) {{'active'}} @endif">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span class="menu-text">Medical Services</span>
                        {{ App\Http\Helpers::getBadgeMedicalServices() }}
                    </a>
                </li>
                <li class="sidebar-dropdown">
                    <a href="{{ route('spaServices') }}" class="@if (Request::is('spaServices')) {{'active'}} @endif">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span class="menu-text">Spa</span>
                        {{ \App\Http\Helpers::getBadgeSpas() }}
                    </a>
                </li>


                <li class="sidebar-dropdown">
                    <a href="{{ route('shopServices') }}" class="@if (Request::is('shopServices')) {{'active'}} @endif">
                        <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        <span class="menu-text">Tienda</span>
                        {{ \App\Http\Helpers::getBadgeShops() }}
                    </a>
                </li>
            </ul>
        </div>

        <div class="sidebar-footer">
            <div>
                <a href="{{ route('logout') }}">
                    <i class="fa fa-power-off"></i> Cerrar Sesión
                </a>
            </div>
        </div>
    </div>
</nav>
