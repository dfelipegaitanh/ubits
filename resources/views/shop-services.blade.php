@extends('layouts.app')

@section('modal')
    @include('modals.modalShops')
@endsection

@section('content')
    <div class="container container-appointment">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="appointment-button text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modalShop">
                        Pedir Tienda
                    </button>
                </div>
                <div class="appointment-header">Tienda</div>
                @foreach($shops as $shop)
                    <div class="appointment-field">
                        <div class="appointment-type">{{ $shop->type }}</div>
                        <div class="appointment-date">{{ $shop->date }} <li class="fa fa-calendar"></li></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
