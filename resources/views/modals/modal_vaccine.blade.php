<div class="modal fade" id="modalVaccine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content-medical-services">
            <div class="modal-header modal-header-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('vaccineServicesPost') }}">
                @csrf
                <input type="hidden" name="pet_id" value="{{ encrypt($pet->id) }}">
                <div class="modal-body modal-body-white">
                    <div class="titleMedicalServices">Pedir Vacuna</div>
                    <div class="form-check">
                        <label>Vacuna:
                            <select class="form-control" name="vaccine_id" required>
                                <option value=""></option>
                                @foreach($allVaccines as $vaccine)
                                    <option value="{{ encrypt($vaccine->id) }}">{{ $vaccine->type }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>

                    <div class="form-check form-check-date">
                        <div class="input-group date">
                            <input type="text" class="form-control" name="date" required><span class="input-group-addon"><i
                                    class="fa fa-calendar"></i></span>
                        </div>
                    </div>

                    <div class="form-check text-right">
                        <button type="submit" class="btn btn-info form-check-button">Pedir Vacuna</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('.input-group.date').datepicker({format: "yyyy/mm/dd", autoclose: true});
</script>
