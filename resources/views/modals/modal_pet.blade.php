<!-- Modal -->
<div class="modal fade modal-dialog modal-xl modal-dialog-centered" id="modalInfoPet" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="field-modal border-bottom">
                    <div>Nombre Mascota</div>
                    <div>{{ $pet->name }}</div>
                </div>
                <div class="field-modal border-bottom">
                    <div>Tipo Mascota</div>
                    <div>{{ $pet->type }}</div>
                </div>
                <div class="field-modal border-bottom">
                    <div>Edad</div>
                    <div>{{ $pet->age }}</div>
                </div>
                <div class="field-modal border-bottom">
                    <div>Tamaño</div>
                    <div>{{ $pet->size }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button-modal button btn-lg btn-block" data-dismiss="modal" aria-label="Close">
                    INICIO
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#modalInfoPet').modal('show')
</script>
