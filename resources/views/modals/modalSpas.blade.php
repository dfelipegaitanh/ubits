<!-- Modal -->
<div class="modal fade modal-dialog modal-xl modal-dialog-centered" id="modalSpa" tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content-medical-services">
            <div class="modal-header modal-header-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('spaServicesPost') }}">
                @csrf
                <input type="hidden" name="pet_id" value="{{ encrypt($pet->id) }}">
                <div class="modal-body modal-body-white">
                    <div class="titleMedicalServices">Pedir Spa</div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="spaService[]" value="Bath Appointment"
                               id="medicalCheck">
                        <label class="form-check-label" for="medicalCheck">
                            Bath Appointment
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="spaService[]" value="Haircut Appointment"
                               id="vaccinationCheck">
                        <label class="form-check-label" for="vaccinationCheck">
                            Haircut Appointment
                        </label>
                    </div>

                    <div class="form-check form-check-date">
                        <div class="input-group date">
                            <input type="text" class="form-control" name="date" required><span class="input-group-addon"><i
                                    class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="form-check text-right">
                        <button type="submit" class="btn btn-info form-check-button">Pedir Spa</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#modalSpa').modal('show');
    $('.input-group.date').datepicker({format: "yyyy/mm/dd", autoclose: true});
</script>
