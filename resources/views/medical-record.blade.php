@extends('layouts.app')

@section('modal')
    @include('modals.modal_pet')
    @include('modals.modal_vaccine')
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="appointment-button text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modalVaccine">
                        Pedir Vacuna
                    </button>
                </div>
                <div class="appointment-header">Vacunas</div>
                @foreach($vaccines as $vaccine)
                    <div class="appointment-field">
                        <div class="appointment-number">{{ $vaccine->number }}</div>
                        <div class="appointment-type">{{ $vaccine->type }}</div>
                        <div class="appointment-date">{{ $vaccine->date }}
                            <li class="fa fa-calendar"></li>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
