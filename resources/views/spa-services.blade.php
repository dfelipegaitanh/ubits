@extends('layouts.app')

@section('modal')
    @include('modals.modalSpas')
@endsection

@section('content')
    <div class="container container-appointment">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="appointment-button text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modalSpa">
                        Pedir Spa
                    </button>
                </div>
                <div class="appointment-header">Spas</div>
                @foreach($spas as $spa)
                    <div class="appointment-field">
                        <div class="appointment-type">{{ $spa->type }}</div>
                        <div class="appointment-date">{{ $spa->date }} <li class="fa fa-calendar"></li></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
