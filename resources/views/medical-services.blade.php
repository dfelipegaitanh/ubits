@extends('layouts.app')

@section('modal')
    @include('modals.modalMedicalServices')
@endsection

@section('content')
    <div class="container container-appointment">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="appointment-button text-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modalMedicalServices">
                        Pedir Cita
                    </button>
                </div>
                <div class="appointment-header">Citas</div>
                @foreach($appointments as $appointment)
                    <div class="appointment-field">
                        <div class="appointment-type">{{ $appointment->type }}</div>
                        <div class="appointment-date">{{ $appointment->date }}
                            <li class="fa fa-calendar"></li>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
