<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', function () {
    return redirect(route('medical-record'));
})->name('home');

Route::get('/medical-record', 'HomeController@medicalRecord')->name('medical-record');
Route::post('/vaccineServices', 'VaccineController@addVaccine')->name('vaccineServicesPost');

Route::get('/medicalServices', 'MedicalServicesController@medicalServices')->name('medicalServices');
Route::post('/medicalServices', 'MedicalServicesController@medicalServicesSave')->name('medicalServicesPost');

Route::get('/spaServices', 'SpaController@spaServices')->name('spaServices');
Route::post('/spaServices', 'SpaController@spaServicesSave')->name('spaServicesPost');

Route::get('/shopServices', 'ShopController@spaServices')->name('shopServices');
Route::post('/shopServices', 'ShopController@spaServicesSave')->name('shopServicesPost');
