<?php

use App\Appointment;
use App\Pet;
use App\Shop;
use App\Spa;
use App\User;
use App\Vaccine;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->fill(
            [
                'name'              => 'ubits',
                'email'             => 'ubits@ubits.com',
                'password'          => bcrypt('secret'),
                'email_verified_at' => now(),
                'remember_token'    => Str::random(10),
            ]
        )
             ->save();

        $user->pet()->create(factory(Pet::class)->make()->toArray());

        factory(User::class, 15)
            ->create()
            ->each(function ( $user ) {
                $user->pet()
                     ->save(factory(Pet::class)->make());
            });

        factory(Vaccine::class, 10)
            ->create();

        $vaccinesCollection = Vaccine::all();
        foreach (Pet::all() as $pet) {

            $pet->appointments()
                ->createMany(
                    factory(Appointment::class, random_int(1, 4))
                        ->make()
                        ->toArray()
                );

            $pet->spas()
                ->createMany(
                    factory(Spa::class, random_int(1, 4))
                        ->make()
                        ->toArray()
                );

            $pet->shops()
                ->createMany(
                    factory(Shop::class, random_int(1, 4))
                        ->make()
                        ->toArray()
                );

            $vaccines = $vaccinesCollection->random(random_int(1, 3));
            foreach ($vaccines as $vaccine) {
                DB::table('pet_vaccine')
                  ->insert(
                      [
                          'pet_id'     => $pet->id,
                          'vaccine_id' => $vaccine->id
                      ]
                  );
            }

        }

    }
}
