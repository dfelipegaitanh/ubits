<?php

/** @var Factory $factory */

use App\Model;
use App\Shop;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Shop::class, function ( Faker $faker ) {
    $type = ['Toys', 'Clothes'];
    return [
        'type' => $type[random_int(0, 1)],
        'date' => $faker->date()
    ];
});
