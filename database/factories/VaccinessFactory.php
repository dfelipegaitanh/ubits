<?php

/** @var Factory $factory */

use App\Vaccine;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Vaccine::class, function ( Faker $faker ) {
    return [
        'number' => $faker->unique()
                          ->randomNumber(2),
        'type'   => $faker->word,
        'date'   => $faker->date()
    ];
});
