<?php

/** @var Factory $factory */

use App\Appointment;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Appointment::class, function ( Faker $faker ) {
    $type = ['Medical Appointment', 'Vaccination Appointment', 'Deworming Appointment'];
    return [
        'type' => $type[random_int(0, 2)],
        'date' => $faker->date()
    ];
});
