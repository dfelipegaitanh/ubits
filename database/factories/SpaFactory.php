<?php

/** @var Factory $factory */

use App\Model;
use App\Spa;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Spa::class, function ( Faker $faker ) {
    $type = ['Bath Appointment','Haircut Appointment'];
    return [
        'type' => $type[random_int(0, 1)],
        'date' => $faker->date()
    ];
});
