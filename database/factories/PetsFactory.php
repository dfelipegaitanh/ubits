<?php

/** @var Factory $factory */

use App\Pet;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Pet::class, function ( Faker $faker ) {
    $petType = ['Cat', 'Dog', 'Fish', 'Other'];
    $petSize = ['Small', 'Medium', 'Big'];
    return [
        'name'        => $faker->name,
        'type'        => $petType[random_int(0, 3)],
        'size'        => $petSize[random_int(0, 2)],
        'description' => $faker->realText(),
        'age'         => $faker->randomNumber(1)
    ];
});
