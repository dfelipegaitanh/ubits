<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function ( Blueprint $table ) {
            $table->id();
            $table->unsignedBigInteger('pet_id');
            $table->enum('type', ['Toys', 'Clothes']);
            $table->date('date');
            $table->timestamps();

            $table->foreign('pet_id')
                  ->references('id')
                  ->on('pets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
